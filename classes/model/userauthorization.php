<?php
namespace Keeper;

class Model_UserAuthorization extends \Crux\Model
{
    protected static $_table_name = 'keeper_user_authorizations';
    protected static $_belongs_to = array(
        'user' => array(
//             'key_from' => 'keeper_user_id',
        ), 
        'resource' => array(
//             'key_from' => 'keeper_resource_id',
        )
    );
}