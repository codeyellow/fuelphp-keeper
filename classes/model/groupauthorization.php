<?php
namespace Keeper;

/**
 * Authorization by Group.
 * 
 * @author AB Zainuddin
 *
 */
class Model_GroupAuthorization extends \Crux\Model
{
    protected static $_table_name = 'keeper_group_authorizations';
    protected static $_belongs_to = array(
        'group' => array(
//             'key_from' => 'keeper_user_id',
        ), 
        'resource' => array(
//             'key_from' => 'keeper_resource_id',
        )
    );
}