<?php
namespace Keeper;

/**
 * User model.
 *
 * @author AB Zainuddin
 */
class Model_User extends \Crux\Model
{
    protected static $_table_name = 'keeper_users'; // also used in validation unique
    protected static $_has_many = array(
        'authorization' => array(
            'model_to' => 'Keeper\\Model_UserAuthorization' 
        ),
        'usergroup' => array(
            'model_to' => 'Keeper\\Model_UserGroup' 
        ) 
    );
    protected static $cruxObservers = array(
        'Keeper\\Observer_HashPassword' => array(
            'events' => array(
                'before_save' 
            ) 
        ) 
    );
    protected static $cruxProperties = array(
        'identifier' => array(
            'validation' => array(
                'required',
                'unique' => array(
                    'keeper_users.identifier' 
                ) ,
                'min_length' => array(5)
            ) 
        ),
        'password' => array(
            'validation' => array(
                'required',
                'min_length' => array(4)
            ) 
        ),
        'is_active' => array(
            'default' => 1
        ) 
    );

    /**
     * Get groups for $userId.
     *
     * @param int $userId
     * @return array Array of Group.
     */
    public static function getGroups($userId = null)
    {
        $result = array();
        
        $user = static::find($userId, array(
            'related' => array(
                'usergroup' => array(
                    'order_by' => array(
                        'usergroup.sequence' => 'asc' 
                    ) 
                ),
                'usergroup.group' 
            ) 
        ));
        
        if ($user) {
            foreach ($user->usergroup as $usergroup) {
                $result[] = $usergroup->group;
            }
        }
        
        return $result;
    }

    /**
     * Set Groups for $userId.
     *
     * @param int $userId
     * @param array $groupIds
     * @return boolean
     */
    public static function setGroups($userId, array $groupIds)
    {
        $success = true;
        
        $sequence = 0;
        $user = static::find($userId, array(
            'related' => array(
                'usergroup' 
            ) 
        ));
        
        if ($user) {
            // Delete all related groups.
            foreach ($user->usergroup as $usergroup) {
                $usergroup->delete();
            }
            
            $user->usergroup = array();
            $success &= $user->save();
            
            // Add related groups.
            foreach ($groupIds as $groupId) {
                $group = Model_Group::find($groupId);
                
                if ($group) {
                    $usergroup = Model_UserGroup::forge();
                    $usergroup->group = $group;
                    $usergroup->sequence = $sequence++;
                    $user->usergroup[] = $usergroup;
                }
            }
            
            $success &= $user->save();
        } else {
            $success = false;
        }
        
        return true;
    }
}