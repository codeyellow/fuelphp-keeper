<?php
namespace Keeper;

class Test_Group extends \TestCase
{

    public function testEmptyName()
    {
        $this->setExpectedException('Orm\\ValidationFailed');
        $group = Model_Group::craft(array());
    }

    public function testDuplicateName()
    {
        $this->setExpectedException('Orm\\ValidationFailed');
        
        $data = array(
            'name' => 'test' . \Str::random(),
        );
        
        $group = Model_Group::craft($data);
        $group = Model_Group::craft($data);
    }
}