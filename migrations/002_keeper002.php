<?php
namespace Fuel\Migrations;

class Keeper002
{

    function up()
    {
        \DBUtil::create_index('keeper_group_authorizations', 'group_id');
        \DBUtil::create_index('keeper_group_authorizations', 'resource_id');
        \DBUtil::create_index('keeper_user_authorizations', 'user_id');
        \DBUtil::create_index('keeper_user_authorizations', 'resource_id');
        \DBUtil::create_index('keeper_user_groups', 'user_id');
        \DBUtil::create_index('keeper_user_groups', 'group_id');
    }

    function down()
    {
        \DBUtil::drop_index('keeper_group_authorizations', 'group_id');
        \DBUtil::drop_index('keeper_group_authorizations', 'resource_id');
        \DBUtil::drop_index('keeper_user_authorizations', 'user_id');
        \DBUtil::drop_index('keeper_user_authorizations', 'resource_id');
        \DBUtil::drop_index('keeper_user_groups', 'user_id');
        \DBUtil::drop_index('keeper_user_groups', 'group_id');
    }
}