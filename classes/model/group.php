<?php
namespace Keeper;

/**
 * Group model.
 *
 * @author AB Zainuddin
 */
class Model_Group extends \Crux\Model
{
    protected static $_table_name = 'keeper_groups';
    protected static $_has_many = array(
        'authorization' => array(
            'model_to' => 'Keeper\\Model_GroupAuthorization' 
        ),
        'usergroup' => array(
            'model_to' => 'Keeper\\Model_UserGroup' 
        ) 
    );
    protected static $cruxProperties = array(
        'name' => array(
            'validation' => array(
                'required',
                'unique' => array(
                    'keeper_groups.name' 
                ) 
            ) 
        ) 
    );
}