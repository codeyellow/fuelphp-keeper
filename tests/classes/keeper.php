<?php
namespace Keeper;

class Test_Keeper extends \TestCase
{

    private function init()
    {
        $props = array(
            'identifier' => 'test' . \Str::random(),
            'password' => 'test' 
        );
        
        $propsResource = array(
            'name' => 'test' . \Str::random() 
        );
        
        $user = Model_User::craft($props);
        $resource = Model_Resource::craft($propsResource);
        $groups = array(
            Model_Group::craft(array(
                'name' => 'test1' . \Str::random() 
            )),
            Model_Group::craft(array(
                'name' => 'test2' . \Str::random() 
            )),
            Model_Group::craft(array(
                'name' => 'test3' . \Str::random() 
            )) 
        );
        
        return array(
            'user' => $user,
            'resource' => $resource,
            'groups' => $groups 
        );
    }

    public function testUserAuthorizationTrue()
    {
        $data = $this->init();
        
        Keeper::setAuthorizationByUser($data['user']->id, $data['resource']->id, Keeper::AUTHORIZATION_TRUE);
        $this->assertEquals(Keeper::AUTHORIZATION_TRUE, Keeper::authorizedByUser($data['user']->id, $data['resource']->id));
    }

    public function testUserAuthorizationFalse()
    {
        $data = $this->init();
        
        Keeper::setAuthorizationByUser($data['user']->id, $data['resource']->id, Keeper::AUTHORIZATION_FALSE);
        $this->assertEquals(Keeper::AUTHORIZATION_FALSE, Keeper::authorizedByUser($data['user']->id, $data['resource']->id));
    }

    public function testUserAuthorizationNotSet()
    {
        $data = $this->init();
        
        Keeper::setAuthorizationByUser($data['user']->id, $data['resource']->id, Keeper::AUTHORIZATION_NOT_SET);
        $this->assertEquals(Keeper::AUTHORIZATION_NOT_SET, Keeper::authorizedByUser($data['user']->id, $data['resource']->id));
    }
    
    public function testSingleGroupAuthorizationTrue()
    {
        $data = $this->init();
        
        $group = \Arr::first($data['groups']);

        Keeper::setAuthorizationByGroup($group->id, $data['resource']->id, Keeper::AUTHORIZATION_TRUE);
        $this->assertEquals(Keeper::AUTHORIZATION_TRUE, Keeper::authorizedByGroup($group->id, $data['resource']->id));
    }

    public function testSingleGroupAuthorizationFalse()
    {
        $data = $this->init();
        
        $group = \Arr::first($data['groups']);
        
        Keeper::setAuthorizationByGroup($group->id, $data['resource']->id, Keeper::AUTHORIZATION_FALSE);
        $this->assertEquals(Keeper::AUTHORIZATION_FALSE, Keeper::authorizedByGroup($group->id, $data['resource']->id));
    }

    public function testSingleGroupAuthorizationNotSet()
    {
        $data = $this->init();
        
        $group = \Arr::first($data['groups']);
        
        Keeper::setAuthorizationByGroup($group->id, $data['resource']->id, Keeper::AUTHORIZATION_NOT_SET);
        $this->assertEquals(Keeper::AUTHORIZATION_NOT_SET, Keeper::authorizedByUser($group->id, $data['resource']->id));
    }
    
    public function testMultiGroupAuthorizationTrue()
    {
        $data = $this->init();

        // Relate user <-> group.
        Model_User::setGroups($data['user']->id, \Arr::pluck($data['groups'], 'id'));
        
        // Set all to true.
        foreach($data['groups'] as $group) {
            Keeper::setAuthorizationByGroup($group->id, $data['resource']->id, Keeper::AUTHORIZATION_TRUE);
        }
        
        $first = \Arr::first($data['groups']);
        $middle = $data['groups'][1];
        $last = \Arr::last($data['groups']);
        
        // Set last to false.
        Keeper::setAuthorizationByGroup($last->id, $data['resource']->id, Keeper::AUTHORIZATION_FALSE);
        $this->assertEquals(Keeper::AUTHORIZATION_FALSE, Keeper::authorizedByUserGroup($data['user']->id, $data['resource']->id));
        
        // Set last to true.
        Keeper::setAuthorizationByGroup($last->id, $data['resource']->id, Keeper::AUTHORIZATION_TRUE);
        $this->assertEquals(Keeper::AUTHORIZATION_TRUE, Keeper::authorizedByUserGroup($data['user']->id, $data['resource']->id));
        
        // Set last to not set.
        Keeper::setAuthorizationByGroup($last->id, $data['resource']->id, Keeper::AUTHORIZATION_NOT_SET);
        $this->assertEquals(Keeper::AUTHORIZATION_TRUE, Keeper::authorizedByUserGroup($data['user']->id, $data['resource']->id));
        
        // Set middle to false.
        Keeper::setAuthorizationByGroup($middle->id, $data['resource']->id, Keeper::AUTHORIZATION_FALSE);
        $this->assertEquals(Keeper::AUTHORIZATION_FALSE, Keeper::authorizedByUserGroup($data['user']->id, $data['resource']->id));
        
        // Set middle to not set.
        Keeper::setAuthorizationByGroup($middle->id, $data['resource']->id, Keeper::AUTHORIZATION_NOT_SET);
        $this->assertEquals(Keeper::AUTHORIZATION_TRUE, Keeper::authorizedByUserGroup($data['user']->id, $data['resource']->id));
        
        // Set first to false.
        Keeper::setAuthorizationByGroup($first->id, $data['resource']->id, Keeper::AUTHORIZATION_FALSE);
        $this->assertEquals(Keeper::AUTHORIZATION_FALSE, Keeper::authorizedByUserGroup($data['user']->id, $data['resource']->id));

        // Set first to not set.
        Keeper::setAuthorizationByGroup($first->id, $data['resource']->id, Keeper::AUTHORIZATION_NOT_SET);
        $this->assertEquals(Keeper::AUTHORIZATION_NOT_SET, Keeper::authorizedByUserGroup($data['user']->id, $data['resource']->id));
    }    
}