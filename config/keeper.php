<?php
return array(
    'phpassIterationCount' => 8,
    'phpassPortable' => true ,
    'autoCreateResource' => true
);

$config['email_config_activation_code'] = array(
    'protocol' => 'smtp',
    'charset' => 'utf-8',
    'smtp_host' => 'mail.markt200.nl',
    'smtp_user' => 'registratie@markt200.nl',
    'smtp_pass' => 'kSu0F20W',
    'smtp_port' => 587,
    'wordwrap' => false,
    'useragent' => '',
    'mailtype' => 'html',
    
    'from_email' => 'registratie@markt200.nl',
    'from_name' => 'Markt200.nl',
    'subject' => 'Registratie' 
);

$config['email_config_password_reset_code'] = array(
    'protocol' => 'smtp',
    'charset' => 'utf-8',
    'smtp_host' => 'mail.markt200.nl',
    'smtp_user' => 'registratie@markt200.nl',
    'smtp_pass' => 'kSu0F20W',
    'smtp_port' => 587,
    'wordwrap' => false,
    'useragent' => '',
    'mailtype' => 'html',
    
    'from_email' => 'registratie@markt200.nl',
    'from_name' => 'Markt200.nl',
    'subject' => 'Wachtwoord vergeten' 
);

$config['email_config_change_email_code'] = array(
    'protocol' => 'smtp',
    'charset' => 'utf-8',
    'smtp_host' => 'mail.markt200.nl',
    'smtp_user' => 'registratie@markt200.nl',
    'smtp_pass' => 'kSu0F20W',
    'smtp_port' => 587,
    'wordwrap' => false,
    'useragent' => '',
    'mailtype' => 'html',
    
    'from_email' => 'registratie@markt200.nl',
    'from_name' => 'Markt200.nl',
    'subject' => 'E-mail verificatie' 
);

$config['auto_create_resource'] = true;
$config['session_login_challenge'] = 'auth.login.challenge';
$config['session_current_user_id'] = 'auth.current.user.id';
$config['session_redirect_url'] = '';
$config['dir_auth'] = DIR_AUTH;
$config['phpass_iteration_count'] = 12;
$config['phpass_portable'] = true;
$config['view_registration_email_reset_password_code'] = DIR_VIEW_TEMPLATE . 'vregistration_email_reset_password_code';
$config['view_registration_email_change_email_code'] = DIR_VIEW_TEMPLATE . 'vregistration_email_change_email_code';
$config['view_registration_email_activation_code'] = DIR_VIEW_TEMPLATE . 'vregistration_email_activation_code';
$config['view_registration_email_password'] = DIR_VIEW_TEMPLATE . 'vregistration_email_password';
$config['method'] = MAuth::METHOD_PHPASS;