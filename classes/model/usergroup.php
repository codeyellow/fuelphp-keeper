<?php
namespace Keeper;

class Model_UserGroup extends \Crux\Model
{
    protected static $_table_name = 'keeper_user_groups';
    protected static $_belongs_to = array(
        'user' => array(
            'model_to' => 'Keeper\\Model_User' 
        ),
        'group' => array(
            'model_to' => 'Keeper\\Model_Group' 
        ) 
    );
}