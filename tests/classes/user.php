<?php
namespace Keeper;

class Test_User extends \TestCase
{

    public function testHashPasswordOnSave()
    {
        $data = array(
            'identifier' => 'test' . \Str::random(),
            'password' => 'test' 
        );
        
        $user = Model_User::craft($data);
        
        $this->assertEquals(true, Keeper::checkPassword('test', $user->password));
        
        // Change password
        $user->password = 'test2';
        $user->save();
        
        $this->assertEquals(true, Keeper::checkPassword('test2', $user->password));
    }
    
    public function testEmptyIdentifier()
    {
        $this->setExpectedException('Orm\\ValidationFailed');
        $user = Model_User::craft(array(
            'password' => 'test' 
        ));
    }

    public function testEmptyPassword()
    {
        $this->setExpectedException('Orm\\ValidationFailed');
        $user = Model_User::craft(array(
            'identifier' => 'test' . \Str::random(),
        ));
    }

    public function testDuplicateIdentifier()
    {
        $data = array(
            'identifier' => 'test' . \Str::random(),
            'password' => 'test' 
        );
        
        $this->setExpectedException('Orm\\ValidationFailed');
        
        $user = Model_User::craft($data);
        $user = Model_User::craft($data);
    }

    public function testIdentifierTooLong()
    {
        $this->setExpectedException('Orm\\ValidationFailed');
        $user = Model_User::craft(array(
            'identifier' => \Str::random('alnum', 129),
            'password' => 'test' 
        ));
    }

    public function testEmptyGroups()
    {
        $random = \Str::random();
        $groupIds = array();
        $groupName = 'testgroup' . $random;
        $props = array(
            'identifier' => 'testcase' . $random,
            'password' => 'test' 
        );
        
        $user = Model_User::craft($props);
        
        $this->assertEquals(array(), $user->getGroups($user->id));
    }

    public function testSetGroups()
    {
        $random = \Str::random();
        $groupIds = array();
        $groupName = 'testgroup' . $random;
        $props = array(
            'identifier' => 'testcase' . $random,
            'password' => 'test' 
        );
        
        $user = Model_User::craft($props);
        
        for($i = 0; $i <= 5; $i++) {
            $group = Model_Group::craft(array(
                'name' => $groupName .= $i 
            ));
            
            $groupIds[] = $group->id;
        }
        
        // Normal sequence
        $user->setGroups($user->id, $groupIds);
        $this->assertEquals($groupIds, \Arr::pluck($user->getGroups($user->id), 'id'));
        
        // Reverse sequence.
        $user->setGroups($user->id, array_reverse($groupIds));
        $this->assertEquals(array_reverse($groupIds), \Arr::pluck($user->getGroups($user->id), 'id'));
    }
}