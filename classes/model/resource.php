<?php
namespace Keeper;

/**
 * All resources in the system.
 *
 * @author AB Zainuddin
 */
class Model_Resource extends \Crux\Model
{
    protected static $_table_name = 'keeper_resources';
    protected static $_has_many = array(
        'userauthorization' => array(
            'model_to' => 'Keeper\\Model_UserAuthorization' 
        ),
        'groupauthorization' => array(
            'model_to' => 'Keeper\\Model_GroupAuthorization' 
        ) 
    );
    protected static $cruxProperties = array(
        'name' => array(
            'validation' => array(
                'required',
                'unique' => array(
                    'keeper_resources.name' 
                ) 
            ) 
        ) 
    );
}