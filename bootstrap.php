<?php
Autoloader::add_classes(array(
    'Keeper\\Keeper' => __DIR__ . DS . 'classes' . DS . 'keeper.php',
    'Keeper\\PasswordHash' => __DIR__ . DS . 'classes' . DS . 'passwordhash.php',
    'Keeper\\Model_Resource' => __DIR__ . DS . 'classes' . DS . 'model' . DS . 'resource.php',
    'Keeper\\Model_User' => __DIR__ . DS . 'classes' . DS . 'model' . DS . 'user.php',
    'Keeper\\Model_Group' => __DIR__ . DS . 'classes' . DS . 'model' . DS . 'group.php',
    'Keeper\\Model_UserAuthorization' => __DIR__ . DS . 'classes' . DS . 'model' . DS . 'userauthorization.php',
    'Keeper\\Model_GroupAuthorization' => __DIR__ . DS . 'classes' . DS . 'model' . DS . 'groupauthorization.php',
    'Keeper\\Model_UserGroup' => __DIR__ . DS . 'classes' . DS . 'model' . DS . 'usergroup.php',
    'Keeper\\Observer_HashPassword' => __DIR__ . DS . 'classes' . DS . 'observer' . DS . 'hashpassword.php',
));