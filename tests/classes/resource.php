<?php
namespace Keeper;

class Test_Resource extends \TestCase
{

    public function testEmptyName()
    {
        $this->setExpectedException('Orm\\ValidationFailed');
        $resource = Model_Resource::craft(array());
    }

    public function testDuplicateName()
    {
        $this->setExpectedException('Orm\\ValidationFailed');
        
        $data = array(
            'name' => 'test' . \Str::random(),
        );
        
        $resource = Model_Resource::craft($data);
        $resource = Model_Resource::craft($data);
    }
}