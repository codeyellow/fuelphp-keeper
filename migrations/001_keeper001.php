<?php
namespace Fuel\Migrations;

class Keeper001
{

    function up()
    {
        \DBUtil::create_table('keeper_users', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'identifier' => array(
                'type' => 'varchar',
                'constraint' => 127 
            ),
            'identifier_unpurified' => array(
                'type' => 'varchar',
                'constraint' => 127 
            ),
            'email' => array(
                'type' => 'varchar',
                'constraint' => 1023 
            ),
            'email_unpurified' => array(
                'type' => 'varchar',
                'constraint' => 1023 
            ),
            'email_new' => array(
                'type' => 'varchar',
                'constraint' => 1023 
            ),
            'email_new_unpurified' => array(
                'type' => 'varchar',
                'constraint' => 1023 
            ),
            'password' => array(
                'type' => 'varchar',
                'constraint' => 63 
            ),
            'is_active' => array(
                'type' => 'tinyint',
                'constraint' => 1,
                'unsigned' => true 
            ),
            'activation_code' => array(
                'type' => 'varchar',
                'constraint' => 31 
            ),
            'reset_password_code' => array(
                'type' => 'varchar',
                'constraint' => 31 
            ),
            'change_email_code' => array(
                'type' => 'varchar',
                'constraint' => 31 
            ),
            'ip' => array(
                'type' => 'varchar',
                'constraint' => 32 
            ),
            'created_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ),
            'updated_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ) 
        ), array(
            'id' 
        ));
        
        \DBUtil::create_table('keeper_groups', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'name' => array(
                'type' => 'varchar',
                'constraint' => 127 
            ),
            'name_unpurified' => array(
                'type' => 'varchar',
                'constraint' => 127 
            ),
            'created_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ),
            'updated_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ) 
        ), array(
            'id' 
        ));
        
        \DBUtil::create_table('keeper_resources', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'name' => array(
                'type' => 'varchar',
                'constraint' => 127 
            ),
            'name_unpurified' => array(
                'type' => 'varchar',
                'constraint' => 127 
            ),
            'created_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ),
            'updated_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ) 
        ), array(
            'id' 
        ));
        
        \DBUtil::create_table('keeper_user_groups', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'user_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'group_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'sequence' => array(
                'type' => 'int',
                'constraint' => 3,
                'unsigned' => true 
            ),
            'created_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ),
            'updated_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ) 
        ), array(
            'id' 
        ));
        
        \DBUtil::create_table('keeper_group_authorizations', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'group_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'resource_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'is_authorized' => array(
                'type' => 'tinyint',
                'constraint' => 1 
            ),
            'created_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ),
            'updated_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ) 
        ), array(
            'id' 
        ));
        
        \DBUtil::create_table('keeper_user_authorizations', array(
            'id' => array(
                'type' => 'int',
                'constraint' => 10,
                'auto_increment' => true,
                'unsigned' => true 
            ),
            'user_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'resource_id' => array(
                'type' => 'int',
                'constraint' => 10,
                'unsigned' => true 
            ),
            'is_authorized' => array(
                'type' => 'tinyint',
                'constraint' => 1,
                'unsigned' => true 
            ),
            'created_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ),
            'updated_at' => array(
                'type' => 'int',
                'constraint' => 11 
            ) 
        ), array(
            'id' 
        ));
        
        // Add default values.
        \DB::insert('keeper_users')->set(array(
            'identifier' => 'admin',
            'identifier_unpurified' => 'admin',
            'password' => '$P$FxuI73u449f2NqRJaR1PdB73CK7z/a/', // test
            'is_active' => '1' 
        ))->execute();
        
        \DB::insert('keeper_groups')->set(array(
            'name' => 'admin' 
        ))->execute();
        
        \DB::insert('keeper_resources')->set(array(
            'name' => 'keeper' 
        ))->execute();
        
        \DB::insert('keeper_user_authorizations')->set(array(
            'user_id' => 1,
            'resource_id' => 1,
            'is_authorized' => 1 
        ))->execute();
    }

    function down()
    {
        \DBUtil::drop_table('keeper_users');
        \DBUtil::drop_table('keeper_groups');
        \DBUtil::drop_table('keeper_resources');
        \DBUtil::drop_table('keeper_user_groups');
        \DBUtil::drop_table('keeper_group_authorizations');
        \DBUtil::drop_table('keeper_user_authorizations');
    }
}