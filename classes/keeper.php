<?php
namespace Keeper;

class Keeper
{
    const AUTHORIZATION_NOT_SET = -1;
    const AUTHORIZATION_FALSE = 0;
    const AUTHORIZATION_TRUE = 1;
    protected static $configLoaded = false;
    protected static $hasher = null;
    protected static $user = null;

    protected static function config($item)
    {
        static::$configLoaded || static::loadConfig();
        
        return \Config::get('keeper.' . $item);
    }

    protected static function loadConfig()
    {
        \Config::load('keeper', true);
        static::$configLoaded = true;
    }

    protected static function loadHasher()
    {
        $iterationCount = static::config('phpassIterationCount');
        $portable = static::config('phpassPortable');
        
        static::$hasher = new PasswordHash($iterationCount, $portable);
    }

    protected static function hasher()
    {
        static::$hasher || static::loadHasher();
        
        return static::$hasher;
    }
    
    protected static function persist($store = false) {
        if($store) {
            \Session::set('keeper', static::$user);
        } else {
            static::$user = \Session::get('keeper', array());
        }
    }

    public static function logout()
    {
        static::$user = null;
        static::persist(true);
    }

    public static function isLoggedIn()
    {
        static::persist();

        return static::$user instanceof Model_User;
    }
    
    /**
     * Hash password.
     *
     * @param string $plain            
     * @return string Hashed password.
     */
    public static function hashPassword($plain)
    {
        return static::hasher()->hashPassword($plain);
    }

    /**
     * Compairs $password from database against $response.
     *
     * @param
     *            string stored_password
     * @param
     *            string submit_password
     * @return bool
     */
    public static function checkPassword($submit_password, $stored_password)
    {
        return static::hasher()->checkPassword($submit_password, $stored_password);
    }

    /**
     * Check credentials and log in if correct.
     *
     * @param
     *            string user_name
     * @param
     *            string password
     * @return bool
     */
    public static function login($identifier, $password)
    {
        if (strlen($identifier) == 0 || strlen($password) == 0) {
            return false;
        }
        
        $user = Model_User::find('first', array(
            'where' => array(
                array(
                    'identifier',
                    $identifier 
                ),
                array(
                    'is_active',
                    1 
                ) 
            ),
            'limit' => 1 
        ));
        
        if ($user && static::checkPassword($password, $user->password)) {
            static::currentUser($user);
            return true;
        }
        
        return false;
    }

    /**
     * Get all authorization for all resources for $user.
     *
     * @param int $user_id            
     * @return array resource_id => Auth_resource_authorization, resource_id,
     *         Auth_resource_authorization
     */
    public static function authorizationByUser($userId)
    {
        // TODO: remove related from query fetch.
        $authorization = Model_UserAuthorization::query()->related('user')->where('user.id', $userId)->where('is_authorized', '!=', static::AUTHORIZATION_NOT_SET)->get();
        
        return $authorization ? $authorization : array();
    }

    /**
     * Get all authorization for all resources for $group.
     *
     * @param int $user_id            
     * @return array resource_id => Auth_resource_authorization, resource_id,
     *         Auth_resource_authorization
     */
    public static function authorizationByGroup($groupId)
    {
        // TODO: remove related from query fetch.
        $authorization = Model_GroupAuthorization::query()->related('group')->where('group.id', $groupId)->where('is_authorized', '!=', static::AUTHORIZATION_NOT_SET)->get();
        
        return $authorization ? $authorization : array();
    }

    /**
     * Get all authorization for all resources for $group.
     *
     * @param int $user_id            
     * @return array resource_id => Auth_resource_authorization, resource_id,
     *         Auth_resource_authorization
     */
    public static function resultingGroupAuthorization($userId)
    {
        $authorization = array();
        $groups = array();
        
        $groups = Model_User::getGroups($userId);
        
        if ($groups) {
            foreach (array_reverse($groups) as $group) {
                foreach (Keeper::authorizationByGroup($group->id) as $groupAuthorization) {
                    $authorization[$groupAuthorization->resource->id] = $groupAuthorization;
                }
            }
        }
        
        return $authorization;
    }

    /**
     * Get all authorization for all resources for $group.
     *
     * @param int $user_id            
     * @return array resource_id => Auth_resource_authorization, resource_id,
     *         Auth_resource_authorization
     */
    public static function resultingUserAuthorization($userId)
    {
        $result = array();
        
        // Find group authorization.
        foreach (static::resultingGroupAuthorization($userId) as $authorization) {
            $result[$authorization->resource_id] = $authorization;
        }
        
        // Overwrite group authorization with user authorization.
        foreach (static::authorizationByUser($userId) as $authorization) {
            $result[$authorization->resource_id] = $authorization;
        }
        
        return $result;
    }

    /**
     * Check if $user is authorized for given $resource.
     *
     * @param string $resource            
     * @param User $user            
     * @return bool True if authorized, false otherwise.
     */
    public static function isAuthorized($resourceName, $userId = null)
    {
        // Add resource.
        if (static::config('autoCreateResource')) {
            try {
                Model_Resource::craft(array(
                    'name' => $resourceName 
                ));
            } catch (\Orm\ValidationFailed $e) {
                // Do nothing.
            }
        }
        
        // Never authorized if not logged in.
        if (!static::isLoggedIn()) {
            return false;
        }
        
        // Get current user.
        is_null($userId) && $userId = static::currentUser()->id;
        
        // Get resource id.
        $resourceId = Model_Resource::query()->where('name', $resourceName)->get_one()->id;
        
        // Get authorizatin by user and group.
        $authorizedByUser = static::authorizedByUser($userId, $resourceId);
        $authorizedByGroup = static::authorizedByUserGroup($userId, $resourceId);
        
        // User auth preceeds group auth.
        switch (true) {
            case $authorizedByUser != static::AUTHORIZATION_NOT_SET :
                return (bool)$authorizedByUser;
                break;
            case $authorizedByGroup != static::AUTHORIZATION_NOT_SET :
                return (bool)$authorizedByGroup;
                break;
            default :
                return false;
                break;
        }
    }

    /**
     * Set/get current user.
     *
     * @param Model_User $user            
     * @return Model_User
     */
    public static function currentUser($user = null)
    {
        static::persist();
        
        if(!is_null($user)) {
            static::$user = $user;
            static::persist(true);
        }
        
        return static::$user;
    }

    /**
     * Check authorization by user.
     *
     * @param string $resourceName            
     * @param User $user            
     * @return int -1 not found, 0 authorized, 1 authorized.
     */
    public static function authorizedByUser($userId, $resourceId)
    {
        $authorization = static::authorizationByUser($userId);
        $authorization = \Arr::pluck($authorization, 'is_authorized', 'resource_id');
        
        return isset($authorization[$resourceId]) ? $authorization[$resourceId] : static::AUTHORIZATION_NOT_SET;
    }

    /**
     * Check authorization by user.
     *
     * @param string $resourceName            
     * @param int $groupId            
     * @return int -1 not found, 0 authorized, 1 authorized.
     */
    public static function authorizedByGroup($groupId, $resourceId)
    {
        $authorization = static::authorizationByGroup($groupId);
        $authorization = \Arr::pluck($authorization, 'is_authorized', 'resource_id');
        
        return isset($authorization[$resourceId]) ? $authorization[$resourceId] : static::AUTHORIZATION_NOT_SET;
    }

    /**
     * Check authorization by group.
     *
     * @param string $resourceName            
     * @param User $user            
     * @return int -1 not found, 0 authorized, 1 authorized.
     */
    public static function authorizedByUserGroup($userId, $resourceId)
    {
        $result = static::AUTHORIZATION_NOT_SET;
        
        $user = Model_User::find($userId);
        
        if ($user && $user->usergroup) {
            foreach ($user->usergroup as $usergroup) {
                // Check group authorization.
                $authorization = static::authorizationByGroup($usergroup->group->id);
                $authorization = \Arr::pluck($authorization, 'is_authorized', 'resource_id');
                
                if (isset($authorization[$resourceId]) && $authorization[$resourceId] !== static::AUTHORIZATION_NOT_SET) {
                    $result = $authorization[$resourceId];
                }
            }
        }
        
        return $result;
    }

    /**
     * Set single authorization by user.
     *
     * @param int $groupId            
     * @param int $resourceId            
     * @param int $isAuthorized            
     */
    public static function setAuthorizationByUser($userId, $resourceId, $isAuthorized)
    {
        $userId = (int)$userId;
        $resourceId = (int)$resourceId;
        $isAuthorized = (int)$isAuthorized;
        
        $userAuthorization = Model_UserAuthorization::query()->where('user_id', $userId)->where('resource_id', $resourceId)->get_one();
        
        if (!$userAuthorization) {
            $user = Model_User::find($userId);
            $resource = Model_Resource::find($resourceId);
            
            // Check if both user and resource exists.
            if (!$user && !$resource) {
                return false;
            }
            
            $userAuthorization = Model_UserAuthorization::forge();
            $userAuthorization->user = $user;
            $userAuthorization->resource = $resource;
            
            $userAuthorization->save();
        }
        
        // TODO: AUTHORIZATION_NOT_SET should delete authorization, except
        // front-end will die.
        switch ($isAuthorized) {
            case static::AUTHORIZATION_NOT_SET :
            case static::AUTHORIZATION_FALSE :
            case static::AUTHORIZATION_TRUE :
                $userAuthorization->is_authorized = $isAuthorized;
                $userAuthorization->save();
                break;
            default :
                // Delete.
                $userAuthorization->delete();
                $userAuthorization = new Model_UserAuthorization();
                break;
        }
        
        return $userAuthorization;
    }

    /**
     * Set single authorization by group.
     *
     * @param int $groupId            
     * @param int $resourceId            
     * @param int $isAuthorized            
     */
    public static function setAuthorizationByGroup($groupId, $resourceId, $isAuthorized)
    {
        $groupId = (int)$groupId;
        $resourceId = (int)$resourceId;
        $isAuthorized = (int)$isAuthorized;
        
        $groupAuthorization = Model_GroupAuthorization::query()->where('group_id', $groupId)->where('resource_id', $resourceId)->get_one();
        if (!$groupAuthorization) {
            $group = Model_Group::find($groupId);
            $resource = Model_Resource::find($resourceId);
            // Check if both user and resource exists.
            if (!$group && !$resource) {
                return false;
            }
            
            $groupAuthorization = Model_GroupAuthorization::forge();
            $groupAuthorization->group = $group;
            $groupAuthorization->resource = $resource;
            
            $groupAuthorization->save();
        }
        
        // TODO: AUTHORIZATION_NOT_SET should delete authorization, except
        // front-end will die.
        switch ($isAuthorized) {
            case static::AUTHORIZATION_NOT_SET :
            case static::AUTHORIZATION_FALSE :
            case static::AUTHORIZATION_TRUE :
                $groupAuthorization->is_authorized = $isAuthorized;
                $groupAuthorization->save();
                break;
            default :
                // Delete.
                $groupAuthorization->delete();
                $groupAuthorization = new Model_UserAuthorization();
                break;
        }
        
        return $groupAuthorization;
    }
}