<?php
/**
 * Purify observer.
 * 
 * TODO: Move htmlpurifier to myorm.
 *
 * @package		myorm
 * @version		1.0
 * @author		AB Zainuddin
 * @copyright	2012 Zaicorp BV
 * @link		http://www.zaicorp.nl
 */
namespace Keeper;

class Observer_HashPassword extends \Orm\Observer
{

    /**
     * Hash password.
     *
     * @param \Orm\Model $obj            
     */
    public function before_save(\Orm\Model $obj)
    {
        if($obj->is_changed('password') && strlen($obj->password) > 0){
            $obj->password = Keeper::hashPassword($obj->password);
        }
    }
}

// End of file createdat.php
